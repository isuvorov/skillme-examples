<?php
header('Content-Type: text/html; charset=utf-8');
/**
 * Created by PhpStorm.
 * User: isuvorov
 * Date: 18.05.14
 * Time: 15:25
 */


$string = "Hello, Привет, World! @#$%";

echo $string."<br>";
echo "strlen($string) = ".strlen($string)."<br>";
echo "mb_strlen($string, \"UTF-8\") = ".mb_strlen($string, "UTF-8")."<br>";
echo "<br>";



for($i = 0; $i < mb_strlen($string, "UTF-8"); $i++){
    //$char = $string[$i];
    $char = mb_substr ( $string , $i, 1,   "UTF-8" );


    echo $i . " " . $char . "<br>";
}
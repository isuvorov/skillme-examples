<?php
/**
 * Created by PhpStorm.
 * User: isuvorov
 * Date: 27.04.14
 * Time: 12:18
 */

/**
 *
 * $num = 0..15
 *
 * 0
 * #000000
 *
 * 1
 * #111111
 *
 *
 * #ffffff
 *
 */

function int2color($num)
{
    if($num < 0 || $num > 15){
        return '#ff0000';
    }

    $char = dechex($num);
    return '#' . str_repeat($char, 6);
    //return '#777777';
}
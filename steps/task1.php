<h1>Hello World</h1>
<?php
for($i = 0; $i < 100; $i++){
	echo $i . ' ';
}

?>

<h2> Пример </h2>

<?php
/*
$world = 'Мир';

echo '<h2> Пример $world </h2>';
echo "<h2> Пример $world </h2>";

for($i = 110; $i < 222; $i++){
	echo $i . ' ';
}
*/

$array = array(
	'andrey' => 23, 
	'alex' => 67, 
	'stepan' => -20, 
	);
	
foreach($array as $key => $value){
	$rub = $value * 33;
	echo "<b>$key</b> : <i>{$value}dollars или {$rub}рублей</i><br>";
}

//var_dump($array);

//$j = '12335';

//var_dump($j);

$file = file_get_contents('logins.txt');

$rows = explode("\n", $file);
?>

<br>

<?php
//почитайте про foreach и ассоциативные массивы
foreach($rows as $key => $row){
	$cells = explode("\t", $row);
	
	$subject = 'Настало время прокачивать свои навыки веб-программирования';
	$email = $cells[0];
	$fio = $cells[1];
	$login = $cells[2];
	$password = $cells[3];
	$domain = $cells[4];
	$vk = $cells[5];
	
	//heredoc строки
$template = <<<STR
Привет, $fio
Настало время прокачивать свои навыки веб-программирования)

Как начать:
1) вот доступы к сайту http://skillme.in/lib/
логин: $login
пароль: $password
ссылка для входа: http://skillme.in/lib/wp-login.php

сначала в это окно http://img580.imageshack.us/img580/7228/ctel.png
нужно ввести
логин: qwerty
пароль: q1w2e3r4
потом уже в окне авторизации wp свои логин и пароль

2) Ты залогинился и теперь можешь начать получать навыки
На странице, на которой ты оказался, есть ссылка на 0 навык работы с FTP
Вот твои доступы к FTP
Хост: $domain
Логин: $login
Пароль: $password (совпадает с паролем wp)

3) Как только ты выполнишь задание — кидай ссылку на результат мне в личку

Удачи)
Ну и пиши, если будут вопросы)
P.S. Пароли никому не раздавай


STR;

$headers = array(); 
$headers[] = "MIME-Version: 1.0"; 
$headers[] = "Content-type: text/plain; charset=utf-8"; 
$headers[] = "From: SkillMe <robot@skillme.net>"; 
//$headers[] = "Bcc: JJ Chong < bcc@domain2.com > "; 
//$headers[] = "Reply-To: Recipient Name <receiver@domain3.com>"; 
$headers[] = "Subject: {$subject}"; 
$headers[] = "X-Mailer: PHP/".phpversion(); 


	$result = mail($email, $subject, $template, implode("\r\n", $headers));
	//var_dump($result);
	echo "На $email : Результат ";
	var_dump($result);
	echo "<br>";
	
	//echo $template;
}

//var_dump($rows);



?>

<?

/*
?>

Привет
Настало время прокачивать свои навыки веб-программирования)

Как начать:
1) вот доступы к сайту http://skillme.in/lib/
логин: gleb
пароль: xULcYU1OCT
ссылка для входа: http://skillme.in/lib/wp-login.php

сначала в это окно http://img580.imageshack.us/img580/7228/ctel.png
нужно ввести
логин: qwerty
пароль: q1w2e3r4
потом уже в окне авторизации wp свои логин и пароль

2) Ты залогинился и теперь можешь начать получать навыки
На странице, на которой ты оказался, есть ссылка на 0 навык работы с FTP
Вот твои доступы к FTP
Хост: gleb.skillme.net
Логин: gleb.skillme
Пароль: xULcYU1OCT (совпадает с паролем wp)

3) Как только ты выполнишь задание — кидай ссылку на результат мне в личку

Удачи)
Ну и пиши, если будут вопросы)
P.S. Пароли никому не раздавай

*/
?>
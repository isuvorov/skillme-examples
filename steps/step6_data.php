<?php
$data = array(
	'air' => array(
		'wifi' => array(
			'16' => 19990,
			'32' => 23990,
			/*'64' => 27990,
			'128' => 31990,*/
		),
		'3g' => array(
			'16' => 24990,
			'32' => 28990,
			'64' => 32990,
			'128' => 36990,
		),
	),
	'ipad2' => array(
		'wifi' => array(
			'16' => 14990,
		),
		'3g' => array(
			'16' => 19990,
		),
	),
	'mini_retina' => array(
		/*'wifi' => array(
			'16' => 15990,
			'32' => 19990,
			'64' => 23990,
			'128' => 27990,
		),*/
		'3g' => array(
			'16' => 20990,
			'32' => 24990,
			'64' => 28990,
			'128' => 32990,
		),
	),
	'mini' => array(
		'wifi' => array(
			'16' => 11990,
		),
		'3g' => array(
			'16' => 16990,
		),
	),
);

$rub_usd = 35.6828;

$rub = $data[$_GET['type']][$_GET['net']][$_GET['size']];
if(!$rub){
	$rub = 0;
}
$result = array(
	'rub' => $rub,
	'usd' => round($rub / $rub_usd, 2),
	'euro' => round($rub / $rub_usd, 2),
);

sleep ( 1 );

echo json_encode($result);

//var_dump($result);
<?php
//http://htmlbook.ru/samlayout/verstka-na-html5
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Пример страницы</title>
  <link href='http://fonts.googleapis.com/css?family=Philosopher|Marck+Script|Exo+2&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <style>
  p { color:  navy; }
  
  /*
  //aaaaa8
  //5a5a58
  
  //8c8c8c text	
  //9ec04f bold
  
  //font-family: 'Marck Script', cursive;
  */
  
  i{	
	color: #9ec04f;
	font-family: 'Marck Script', cursive;
  }
  
  h1, h2, h3, h4, h5, h6 {
	font-family: 'Philosopher', sans-serif;
  }
  body{
	font-family: 'Exo 2', sans-serif;
	background:#ebebeb;
	color: #8c8c8c;
  }
  a,
  a:visited{
	color: #aaaaa8;
	text-decoration:none;
  }
  a:hover,
  a:active
  
  {
	color: #5a5a58;
	text-decoration:underline;
  
  }
  
  .body{
	width:1000px;
	margin-top:0px;
	margin-left:auto;
	margin-right:auto;
	min-height:400px;
  
  }
  
  .body, .body * {
	/*border:1px red solid;*/
  }
  
  .logo_td{
	width:200px; 
  }
  .logo_td img{
	width:200px; 
  }
  .header_table{
	width:100%;
  }
  .menu_td{
	font-size:16pt; /* pt or px */
	text-align:right;
  }
  .menu_td a {
	margin-left:15px;
  }
  .promo{
	margin-top:30px;
  }
  .promo img{
	width:800px;
  }
  .promo_wrapper{
	background:white;
	width:95%;
	margin:0 auto;
	text-align:center;
	
	-webkit-border-radius: 20px;
	-moz-border-radius: 20px;
	border-radius: 20px;
	
	-webkit-box-shadow: 0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	-moz-box-shadow:    0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	box-shadow:         0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	
  }
  .lead{
	text-align:center;
	font-size:150%;
  }
  
  .page_header{
	background: url('http://www.boozallen.com/media/image/background-header-earth-from-space.png');
	background-position: right top;
	background-repeat: no-repeat; 
  }
  
  #super{
	font-size:200%;
  }
  </style>
 </head>
 <body>
	<div class="body">
		<header class="page_header">
			<table class="header_table">
				<tr>
					<td class="logo_td">
						<a href=".">
							<img src="http://www.vectortemplates.com/raster/batman-logo-big.gif">
						</a>
					</td>
					<td class="menu_td">
						<a href="#qwe">Главная</a>
						<a href="#123">О нас</a>
						<a href="#qweasd">Услуги</a>
						<a href="#">Портфолио</a>
						<a href="#">Контакты</a>
					</td>
				</tr>
			</table>
		
		<!--
			<div style="display:inline-block;float:left:">
				Logo
			</div>
			<div style="display:inline-block;float:right:">
				Menu
			</div>
		-->
		</header>
		<div class="promo">
			<div class="promo_wrapper">
				<img src="http://marketing.blogs.ie.edu/files/2013/04/danone-logo.gif" >
			</div>
			
		</div>
		<div class="lead">
			<h2>Hello Batman</h2>
			Вы заметите, что пространство шириной в один знак <i>(margin)</i> было оставлено около заголовка второго уровня,а сам заголовок стал толще в результате padding, равного 3 знакам.

			Вы можете задавать <i>margin и padding</i> для четырех сторон элемента по отдельности: margin-top, margin-right, <i id="super">margin-bottom</i>, margin-left, padding-top, padding-right, padding-bottom и padding-left.
			
			<div class="ipad_form">
				<h2>Выбери iPad</h2>
				<select id="type">
					<option>air</option>
					<option>ipad2</option>
					<option>mini_retina</option>
					<option>mini</option>
				</select>
				<select id="net">
					<option>wifi</option>
					<option>3g</option>
				</select>
				<select id="size">
					<option>16</option>
					<option>32</option>
					<option>64</option>
					<option>128</option>
				</select>
				<button id="get_price">
					Узнать цену
				</button>
				<div id="prices"><br>
					Цена товара:
					<i><span id="price_usd">12220</span>$</i>   
					<span id="price_rub">12220</span>р
				</div>
				<p id="loading">
					<img width=64 src="http://sbkmag.ru/bitrix/components/terrashop/calendar.of.events/js/loading.gif">
					<br>
					Загрузка
				</p>
				<h1 id="error">
					На складе отсутствует
				</h1>
				
				<br><br><br><br><br>
				<h1 id="current_price">
					12220
				</h1>
			</div>
			
	<div id="defaultCountdown"></div>
	<p>Counting down to 26 January <span id="year">2014</span>.</p>
		</div>
	</div>
	<style>
	.ipad_form{
		padding:20px;
	}
	#loading{
		display:none;
	}
	#prices{
		display:none;
	}
	#current_price	{
		display:none;
	}
	#error	{
		display:none;
	}
	/*
	.lead i{
		color:blue;
	}*/
	</style>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
		
		//alert(123123);
		// использование Math.round() даст неравномерное распределение!
		function getRandomInt(min, max)
		{
		  return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		
		var colors = ['red', 'green', 'blue', 'yellow', 'brown', 'gray'];
		
		for(var i = 0; i < 3; i++){
			console.log(i);
		}
		$(function(){
			//$('.lead i').css( "background-color", "red" );
			$('.lead i').click( function(){
				var i = getRandomInt(0, colors.length - 1);
				console.log(i);
				var color = colors[i];
				$(this).css( "background-color", color );
				
				var j = getRandomInt(0, colors.length - 1);
				if(i == j){
					j = getRandomInt(0, colors.length - 1);
				}
				//console.log(i);
				var color = colors[j];
				$(this).css( "color", color );
					
				
				
		
				
				
				console.log('working');
			});
			
			$('#get_price ').click(function (){
			
				var params = {
					type: $('#type').val(), 
					net: $('#net').val(), 
					size: $('#size').val(), 
				}
				
				$('#prices').hide();
				$('#loading').show();
				$('#error').hide();
				/*
				$.get( "http://steps.ru/step6_data.php", params ).done(function( data ) {
					$('#current_price').html(data);
					$('#current_price').show();
					$('#loading').hide();
					
				});*/
				$.getJSON( "http://steps.ru/step6_data.php", params ).done(function( data ) {
					if(data.rub == 0){
						$('#loading').hide();
						$('#error').show();
						return 1;
					}
					$('#price_usd').html(data.usd);
					$('#price_rub').html(data.rub);
					$('#prices').show();
					$('#loading').hide();
					$('#error').hide();
					
				});
				
				
				
				//http://steps.ru/step6_data.php?type=air&net=wifi&size=16
				//alert('click ');
			
			});
			
			$( ".ipad_form select" ).change(function() {
				$('#get_price').click();
			  //alert( "Handler for .change() called." );
			});
			
		});
	</script>
	
	<script src="http://keith-wood.name/js/jquery.plugin.js"></script>
	<script src="http://keith-wood.name/js/jquery.countdown.js"></script>
	<script>
	$(function () {
		var austDay = new Date();
		austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
		$('#defaultCountdown').countdown({until: austDay});
		$('#year').text(austDay.getFullYear());
	});
	</script>
 </body>
</html>

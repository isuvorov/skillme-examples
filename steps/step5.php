<?php
//http://htmlbook.ru/samlayout/verstka-na-html5
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Пример страницы</title>
  <link href='http://fonts.googleapis.com/css?family=Philosopher|Marck+Script|Exo+2&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <style>
  p { color:  navy; }
  
  /*
  //aaaaa8
  //5a5a58
  
  //8c8c8c text	
  //9ec04f bold
  
  //font-family: 'Marck Script', cursive;
  */
  
  i{	
	color: #9ec04f;
	font-family: 'Marck Script', cursive;
  }
  
  h1, h2, h3, h4, h5, h6 {
	font-family: 'Philosopher', sans-serif;
  }
  body{
	font-family: 'Exo 2', sans-serif;
	background:#ebebeb;
	color: #8c8c8c;
  }
  a,
  a:visited{
	color: #aaaaa8;
	text-decoration:none;
  }
  a:hover,
  a:active
  
  {
	color: #5a5a58;
	text-decoration:underline;
  
  }
  
  .body{
	width:1000px;
	margin-top:0px;
	margin-left:auto;
	margin-right:auto;
	min-height:400px;
  
  }
  
  .body, .body * {
	/*border:1px red solid;*/
  }
  
  .logo_td{
	width:200px; 
  }
  .logo_td img{
	width:200px; 
  }
  .header_table{
	width:100%;
  }
  .menu_td{
	font-size:16pt; /* pt or px */
	text-align:right;
  }
  .menu_td a {
	margin-left:15px;
  }
  .promo{
	margin-top:30px;
  }
  .promo img{
	width:800px;
  }
  .promo_wrapper{
	background:white;
	width:95%;
	margin:0 auto;
	text-align:center;
	
	-webkit-border-radius: 20px;
	-moz-border-radius: 20px;
	border-radius: 20px;
	
	-webkit-box-shadow: 0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	-moz-box-shadow:    0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	box-shadow:         0px 6px 17px 0px rgba(50, 50, 50, 0.74);
	
  }
  .lead{
	text-align:center;
	font-size:150%;
  }
  
  .page_header{
	background: url('http://www.boozallen.com/media/image/background-header-earth-from-space.png');
	background-position: right top;
	background-repeat: no-repeat; 
  }
  
  #super{
	font-size:200%;
  }
  </style>
 </head>
 <body>
	<div class="body">
		<header class="page_header">
			<table class="header_table">
				<tr>
					<td class="logo_td">
						<a href=".">
							<img src="http://www.vectortemplates.com/raster/batman-logo-big.gif">
						</a>
					</td>
					<td class="menu_td">
						<a href="#qwe">Главная</a>
						<a href="#123">О нас</a>
						<a href="#qweasd">Услуги</a>
						<a href="#">Портфолио</a>
						<a href="#">Контакты</a>
					</td>
				</tr>
			</table>
		
		<!--
			<div style="display:inline-block;float:left:">
				Logo
			</div>
			<div style="display:inline-block;float:right:">
				Menu
			</div>
		-->
		</header>
		<div class="promo">
			<div class="promo_wrapper">
				<img src="http://marketing.blogs.ie.edu/files/2013/04/danone-logo.gif" >
			</div>
			
		</div>
		<div class="lead">
			<h2>Hello Batman</h2>
			Вы заметите, что пространство шириной в один знак <i>(margin)</i> было оставлено около заголовка второго уровня,а сам заголовок стал толще в результате padding, равного 3 знакам.

			Вы можете задавать <i>margin и padding</i> для четырех сторон элемента по отдельности: margin-top, margin-right, <i id="super">margin-bottom</i>, margin-left, padding-top, padding-right, padding-bottom и padding-left.
		
		</div>
	</div>
 </body>
</html>
